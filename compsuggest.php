<?php
/* 
 * File: compsuggest.php
 * Date: 2010.Jul.14
 * Site: letter.x design
 * Notes:
 *   $ToEmails is a string containing email address that the message is sent to. This can hold multiple email addresses seperated by commas
 *   $FromEmail is a string containing email address that will be present in the From Field. This email address can really be anything, But inorder to be able to reply to the emails easily, this can be set to the supplied email address.
 *   $Subject is a string containing the subject line of the email
 *   $Body is a string that contains the content including $name, $email, $phone, and $message
 *   $FromField is a switch
 *   Header(Location: xxxxx.xxx) at the bottom designates which page the user is passed after their message has been sent
 * */

$ToEmails = "suggestedcompliment@letterxdesign.com";
$FromEmail = "letterxdesigndotcom@letterxdesign.com";
$Subject = "New message from contact page at letterxdesign.com";
$Subject = "Message from letterxdesign.com";
$today = date('Y.M.d - G:i:s T');
$Body = "";
/* Switch the location of the backslashes to change */
//$FromField = 0; // This sets From Email address to generic From Address
$FromField = 1; // This sets From Email to the address supplied by the commenter


if(isset($_POST['Submit']))
{
	/* Gather Variables from the posted form */
	$name = $_POST['name'];
	$email = $_POST['email'];
	$message = $_POST['msg'];
	
	/* Formulate the body of the email */
	$Body = "
	Name: $name
	Email: $email
	
	$message
	
	End of Message
	This messege was generated using the compliment suggest page at letterxdesign.com
	";
	
	stripslashes($message);
	
	/* If an easy reply to option is desired then this changes the From field to the email address supplied by the commenter */
	/* Also checks that the user did supply an email address, if not than the from email address does not change */
	if($FromField == 1) { if(empty($email)) { } else { $FromEmail = $email; } }
	
	$send = mail($ToEmails,"$Subject $today",$Body,"FROM: $FromEmail");
	
	if(!$send) { echo "There was an error processing your email address. We Apologize for the mistake."; }
	
	// This is the file that is loaded after the emails have been processed
	header("Location: suggested.html");	

	
} else { echo "<SCRIPT>history.back(1);</SCRIPT>"; }

?>