var today = new Date();
today.setHours(0);
today.setMinutes(0);
today.setSeconds(0);
console.log(today);
var millisPerDay = 1000 * 60 * 60 * 24;
var millisPerYear = 1000 * 60 * 60 * 24 * 365;

var monthNames = [
	"January",
	"February",
	"March",
	"April",
	"May",
	"June",
	"July",
	"August",
	"September",
	"October",
	"November",
	"December"
];

var displayDate = function(date) {
	this.date = date;
	var yr = date.getFullYear();
	var mon = monthNames[date.getMonth()];
	var day = date.getDate();
	return mon + " " + day + ", " + yr;
	console.log(mon + " " + day + ", " + yr)
}
var daysOld = function() {
	var diff = (today - bday) / millisPerDay;
	return Math.floor(diff + 1);
}
var yearsOld = function(on) {
	var diff = (on.getFullYear() - bday.getFullYear());
	if (on.getMonth() < bday.getMonth()) {
		return diff - 1;
	} else if (on.getMonth() > bday.getMonth()) {
		return diff;
	} else if (on.getMonth() == bday.getMonth() && on.getDate() < bday.getDate()) {
		return diff - 1;
	} else {
		return diff;
	}
}
var dateOf = function (span) {
	var dateIs = new Date ((span * millisPerDay) + bday.getTime());
	return displayDate(dateIs) + " (" + yearsOld(dateIs) + " yrs)";
}