<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8 />
<title>Days Old!</title>
<link rel="Shortcut Icon" type="image/ico" href="/images/x.ico" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<style type="text/css">
html, body {font-family: Helvetica, Arial, sans-serif; width: 95%; max-width: 960px; margin: 0 auto;}
</style>
<script type="text/javascript" src="daysOld.js"></script>
<?php
$bday = $_GET ['bday'];
$bdate = date_parse($bday);
?>
<!-- BEGIN GOOGLE ANALYTICS TRACKING CODE -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-15847880-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- END GOOGLE ANALYTICS TRACKING CODE -->
</head>
<body>
	<script>var bday = new Date (<?php echo (strtotime($bdate['month'] . "/" . $bdate['day'] . "/" . $bdate['year']) + (24 * 60 * 60)) * 1000 ?>);</script>
	<h3>THE RESULTS</h3>
	<p>Today is <script>document.write(displayDate(today));</script>, and you said you were born <script>document.write(displayDate(bday));</script>.</p>
	<p>That makes you <strong><script>document.write(daysOld());</script></strong> days old.</p>
	<h3>(IN)SIGNIFICANT DATES</h3>
	<p>For no good reason, here are some dates from your past/future and their associated significant quantities of days after your birth.</p>
	<table>
		<tbody>
			<tr>
				<th>Quantity of Days</th>
				<th>Date</th>
			</tr>
			<tr>
				<td>1,000</td>
				<td><script>document.write(dateOf(1000));</script></td>
			</tr>
			<tr>
				<td>5,000</td>
				<td><script>document.write(dateOf(5000));</script></td>
			</tr>
			<tr>
				<td>10,000</td>
				<td><script>document.write(dateOf(10000));</script></td>
			</tr>
			<tr>
				<td>15,000</td>
				<td><script>document.write(dateOf(15000));</script></td>
			</tr>
			<tr>
				<td>20,000</td>
				<td><script>document.write(dateOf(20000));</script></td>
			</tr>
			<tr>
				<td>25,000</td>
				<td><script>document.write(dateOf(25000));</script></td>
			</tr>
			<tr>
				<td>30,000</td>
				<td><script>document.write(dateOf(30000));</script></td>
			</tr>
			<tr>
				<td>35,000</td>
				<td><script>document.write(dateOf(35000));</script></td>
			</tr>
		</tbody>
	</table>
</body>
</html>