<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8 />
<title>Days Old!</title>
<link rel="Shortcut Icon" type="image/ico" href="/images/x.ico" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<style type="text/css">
html, body {font-family: Helvetica, Arial, sans-serif; width: 95%; max-width: 960px; margin: 0 auto;}
h3 {margin: 30px 0 0 0;}
p {margin: 10px 0 5px 0;}
</style>
<?php
$bday = new DateTime ($_GET ['bday']);
$today = new DateTime ("now");
$daysOld = $bday->diff($today);
function showDate ($date) {
	echo $date->format('F j, Y');
}
function addDays ($numDays) {
	$bday = new DateTime ($_GET ['bday']);
	$newDate = $bday;
	$newDate->add(new DateInterval('P'.$numDays.'D'));
	showDate($newDate);
	echo " (" . yearsOld($newDate) . " yrs old)";
}
function yearsOld($when) {
	global $bday;
	$yearsOld = $bday->diff($when);
	return $yearsOld->format('%y');
}
$funDays = array(
	1000,
	5000,
	10000,
	15000,
	20000,
	25000,
	30000,
	35000
);

?>
<!-- BEGIN GOOGLE ANALYTICS TRACKING CODE -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-15847880-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- END GOOGLE ANALYTICS TRACKING CODE -->
</head>
<body>
	<h3>THE RESULTS</h3>
	<p>Today is <?php showDate($today); ?>, and you said you were born <?php showDate($bday); ?>.</p>
	<p>That makes you <strong><?php echo $daysOld->format('%a'); ?></strong> days old.</p>
	<?php
	//	yearsOld($today);
	?>
	<h3>(IN)SIGNIFICANT DATES</h3>
	<p>For no good reason, here are some dates from your past/future and their associated significant quantities of days after your birth.</p>
	<table>
		<tbody>
			<tr>
				<th>Qty of Days</th>
				<th>Date</th>
			</tr>
			<?php
				for ($i=0; $i<count($funDays); $i++) { ?>
					<tr>
						<td>
							<?php echo $funDays[$i]; ?>
						</td>
						<td>
							<?php addDays($funDays[$i]); ?>
						</td>
					</tr>
				<?php }
			?>
		</tbody>
	</table>
	<h3>HAVE A FAVORITE NUMBER?</h3>
	<p>If you'd like to know which day you'll be some certain number of days old, just type in the number of days here:</p>
	<form method="POST" action="">
		<label for="thisMany">Pick a number, any number...</label>
		<input type="text" id="thisMany" name="thisMany" required autofocus>
		<input type="submit" name="submit" value="This Number">
	</form>
	<?php 
	if(isset($_POST['submit'])) {
		echo "You'll be <strong>" . $_POST['thisMany'] . "</strong> days old on <strong>";
		addDays($_POST['thisMany']);
		echo "</strong>.";
	}
	?>
</body>
</html>