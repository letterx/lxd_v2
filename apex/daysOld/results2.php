<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8 />
<title>How Old???</title>
<link rel="Shortcut Icon" type="image/ico" href="/images/x.ico" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<style type="text/css">
html, body {font-family: Helvetica, Arial, sans-serif; width: 95%; max-width: 960px; margin: 0 auto;}
h3 {margin: 30px 0 0 0;}
p {margin: 10px 0 5px 0;}
</style>
<?php
$bday = new Birthday ($_GET ['bday'] . " " . $_GET ['time']);
$now = new DateTime ("now");
$secsIn = array(
	array(1, "second"),
	array(60, "minute"),
	array(60 * 60, "hour"),
	array(60 * 60 * 24, "day"),
	array(60 * 60 * 24 * 7, "week"),
	array(60 * 60 * 24 * 7 * 4.35, "month"),
	array(60 * 60 * 24 * 7 * 4.35 * 12, "year")
);
$daysIn = array(
	array(1 * 24 * 60 * 60, "second"),
	array(1 * 24 * 60, "minute"),
	array(1 * 24, "hour"),
	array(1, "day"),
	array(1 / 7, "week"),
	array(1 / 7 / 4.35, "month"),
	array(1 / 7 / 4.35 / 12, "year")	
);
class Birthday extends DateTime {
	public function sToMidnight() {
		$temp = clone $this;
		$temp->add(new DateInterval('P1D'));
		$temp->setTime(00,00,00);
		return $temp->getTimestamp() - $this->getTimestamp();
	}
	public function sSinceMidnight($when) {
		$temp = clone $when;
		$temp->setTime(00,00,00);
		$hours = $when->format('H');
		$minutes = $when->format('i');
		$seconds = $when->format('s');
		$totSecs = ((($hours * 60) + $minutes) * 60) + $seconds;
		return $totSecs;
	}
	public function age($when, $units, $secsIn) {
		$temp = clone $this;
		if ($temp::diff($when)->format('%a') > 0) {
			$daysBetween = $temp::diff($when)->format('%a') - 1;
			$output = (($daysBetween * 24 * 60 * 60) + $this->sToMidnight() + $this->sSinceMidnight($when)) / $secsIn[$units][0];
		} else if ($temp::diff($when)->format('%a') == 0) {
			if($temp->format('d') < $when->format('d')) {
				$daysBetween = 0; // if they were born yesterday
				$output = (($daysBetween * 24 * 60 * 60) + $this->sToMidnight() + $this->sSinceMidnight($when)) / $secsIn[$units][0];
			} else {
				$output = ($when->getTimestamp() - $temp->getTimestamp()) / $secsIn[$units][0]; // if they were born today
			}
		}
		if ($output == 1) {
			if($units < 5) {
				return number_format($output, 0, ".", ",") . " " . $secsIn[$units][1];	
			} else {
				return number_format($output, 2, ".", ",") . " " . $secsIn[$units][1];	
			}
		} else {
			if($units < 5) {
				return number_format($output, 0, ".", ",") . " " . $secsIn[$units][1] . "s";	
			} else {
				return number_format($output, 2, ".", ",") . " " . $secsIn[$units][1] . "s";
			}
		}
	}
	public function dateAfter($increment, $units, $daysIn, $secsIn) {
		$temp = clone $this;
		if($units > 5) {
			$temp->add(new DateInterval('P'.$increment.'Y'));
		} else {
			$incDays = floor($increment / $daysIn[$units][0]);
			$temp->add(new DateInterval('P'.$incDays.'D'));
			if($units < 4) {
				$extraSecs = ($increment % $daysIn[$units][0]) * $secsIn[$units][0];
				$temp->add(new DateInterval('PT'.$extraSecs.'S'));
			}	
		}
		return $temp->format('F j, Y h:i:sa');
	}
}
$funIncs = array(
	1000,
	5000,
	10000,
	15000,
	20000,
	25000,
	30000,
	35000
);
?>
</head>
<body>
	<?php
	/*	$future = new DateTime('2038-05-20 12:00:15');
		$fromTime = new DateTime;
		$fromTime->setTimestamp('326251200');
		echo $fromTime->format('F j, Y h:i:sa') . "<br />";
		echo "------------<br/>";
		echo $bday->age($now, '0', $secsIn) . "<br />";
		echo $bday->dateAfter('5', '0', $daysIn, $secsIn) . "<br />";
	*/
		if($bday > $now) {
			echo "<h3>Oh dear me...</h3>";
			echo "<p>It appears that you've discovered time travel, and were born at some point after what I and my associated have come to describe as &quot;now&quot;. I'm not sure what you expected to find out here, but I'm afraid I'm quite unlikely to deliver it. Would you care instead to <a href='index.php'>enter another date</a>?";
			echo "<style type='text/css'>#results { display: none; }</style>";
		}
	?>
	<div id="results">
	<h6>NOTE: I have not yet considered timezones in this revision, so for the moment, you'll need to make manual adjustments to the data submitted or retrieved to accomodate for your appropriate birth and current location timezones. My server is in the pacific timezone, where it is currently <?php echo $now->format('F j, Y h:i:sa') ?></h6>
	<h3>THE RESULTS</h3>
	<p>You were born <?php echo $bday->format('F j, Y') . " at " . $bday->format('h:i:sa') ?>.</p>
	<p>As of <?php echo $now->format('F j, Y') . " at " . $now->format('h:i:sa') ?>, you are <strong><?php echo $bday->age($now, '3', $secsIn); ?></strong> old.</p>
	<p>Here is your age translated to some less useful units of time:</p>
	<table>
		<tbody>
			<?php
				for ($i = 0; $i < count($secsIn); $i++) {
					echo "<tr><td align='right'>" . $bday->age($now, $i, $secsIn) . "</td></tr>";
				}
			?>
		</tbody>
	</table>
	<h3>(IN)SIGNIFICANT DATES</h3>
	<p>For no good reason, you can also see some dates from your past/future and their associated significant quantities of units after your birth.</p>
	<form method="POST" action="">
		<label for="in">Show me this ridiculousness in</label>
		<select id="in" name="in">
			<?php
				for($i = 0; $i < count($secsIn); $i++) {
					echo "<option value='" . $i . "'>" . $secsIn[$i][1] . "s</option>";
				}
			?>
		</select>
		<input type="submit" name="inSelector" value="->">
	</form>
	<table>
		<tbody>
			<?php
				if(!empty($_POST['inSelector'])) {
					$in = ($_POST['in']);
					for ($i = 0; $i < count($funIncs); $i++) {
						echo "<tr><td><strong>" . number_format($funIncs[$i]) . " " . $secsIn[$in][1] . "s</strong> on " . $bday->dateAfter($funIncs[$i], $in, $daysIn, $secsIn) . "</td></tr>";
					}
				}
			?>
		</tbody>
	</table>
	<h3>HAVE A FAVORITE NUMBER?</h3>
	<p>If you'd like to know which day you'll be some certain number of days old, just type in the number of days here:</p>
	<form method="POST" action="">
		<label for="thisMany">Pick a number, any number...</label>
		<input type="text" id="thisMany" name="thisMany" required autofocus>
		<input type="submit" name="customDay" value="This Number">
	</form>
	<?php 
	if(isset($_POST['customDay'])) {
		// echo "You'll be <strong>" . $_POST['thisMany'] . "</strong> days old on <strong>";
		// addDays($_POST['thisMany']);
		// echo "</strong>.";
		$thisMany = $_POST['thisMany'];
		echo "<table><tbody>";
		for ($i = 0; $i < count($daysIn); $i++) {
			echo "<tr><td>" . number_format($thisMany, 0, ".", ",") . " <strong>" . $secsIn[$i][1] . "s</strong> on " . $bday->dateAfter($thisMany, $i, $daysIn, $secsIn) . "</td>";
		}
		echo "</tbody></table>";
	}
	?>
	</div><!-- #results -->
</body>
</html>