var howMany = function(max) {

  check = [];

  for(i = 0; i < max; i++) {
    check[i] = false;
  }

  var checkAll = function() {
    var complete = false;
    for(ci = 0; check[ci] === true; ci++) {
      if(ci === max - 1) {
        complete = true;
      } else {
        complete = false;
      }
    }
    return complete;
  };
  
  for(i = 1; checkAll() === false; i++) {
    var rdm = Math.floor(Math.random() * max + 1);
    check[rdm - 1] = true;
  }
  return i;
};
/* howMany(5);
console.log("It took " + i + " iterations to randomly generate each of the numbers.");
for(i = 0; i < maxNum; i++) {
  console.log((i + 1) + " is showing " + check[i]);
}
console.log(check); */
var batchSize = 100;
var batch = function (max) {
  var sum = 0;
  for (x = 0; x < batchSize; x++) {
    sum += howMany(max);
  };
  var avg = sum / batchSize;
  return avg;
}