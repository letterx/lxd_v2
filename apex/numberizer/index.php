<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8 />
<title>Numberizer!</title>
<link rel="Shortcut Icon" type="image/ico" href="/images/x.ico" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<style type="text/css">
html, body {font-family: Helvetica, Arial, sans-serif; width: 95%; max-width: 960px; margin: 0 auto;}
</style>
<!-- BEGIN GOOGLE ANALYTICS TRACKING CODE -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-15847880-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- END GOOGLE ANALYTICS TRACKING CODE -->
</head>
<body>
	<h3>THE NUMBERIZER</h3>
	<p>Choose a number, I'll randomly generate numbers lower than your number and see how many tries to takes to get each integer between 1 and your maximum number.</p>
	<form id="number" method="GET" action="results.php">
		<input name="num" autofocus placeholder="Choose a maximum number">
		<input type="submit" value="Submit">
	</form>
</body>
</html>