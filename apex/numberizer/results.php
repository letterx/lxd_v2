<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8 />
<title>Numberizer!</title>
<link rel="Shortcut Icon" type="image/ico" href="/images/x.ico" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<style type="text/css">
html, body {font-family: Helvetica, Arial, sans-serif; width: 95%; max-width: 960px; margin: 0 auto;}
</style>
<script type="text/javascript" src="iterations.js"></script>
<?php
$num = $_GET ['num'];
$tooBig = false;
if ($num > 1000) {
	$tooBig = true;
	$newNum = rand (1,1000);
}
?>
<!-- BEGIN GOOGLE ANALYTICS TRACKING CODE -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-15847880-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- END GOOGLE ANALYTICS TRACKING CODE -->
</head>
<body>
	<?php
		if ($tooBig === true) {
			echo "<h3>TOO LONG KLANKY, TOO LONG!</h3><p>Ok ok ok... I know I didn't say you had to pick a number within a specific range, but I'm not experienced enough as a developer to know what trying to determine the necessary iterations up to " . $num . " would end up doing to my server. I've chose a new lower number at random for you, and that number is " . $newNum . ".</p>";
			$useNum = $newNum;
		} else {
			$useNum = $num;
		}
		echo "<script>num = " . $useNum . ";</script>";
	?>
	<h3>THE RESULTS</h3>
	<p>It took me <script>document.write(howMany(num));</script> iterations to randomly select all numbers between 1 and <?php echo $useNum; ?>.</p>
	<h3>AFTER <script>document.write(batchSize)</script> RUNS</h3>
	<p>We also ran this <script>document.write(batchSize)</script> other times, and determined that the average number of iterations necessary for the number <?php echo $useNum ?> is <script>document.write(batch(num));</script>.</p>
	<p><?php
		echo '<a href="results.php?num=' . $useNum . '">'
	?>
	Try again with <?php echo $useNum ?></a> or <a href="index.php">pick a different number</a>.</p>
</body>
</html>